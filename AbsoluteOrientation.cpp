#include "AbsoluteOrientation.h"

AbsoluteOrientation::AbsoluteOrientation() {

}

void AbsoluteOrientation::computeAO(string filename_objectCoords, string filename_ModelCoords) {
	//load in the ground(object) control points
	objectCoordinates.loadCoordinates(filename_objectCoords);
	objectCoordinates.splitCoordinates();

	//load in the model coordinates
	modelCoordinates.loadCoordinates(filename_ModelCoords);
	modelCoordinates.splitCoordinates();

	//variables where type isn't declared are member variables of class in case
	//the instance of this class needs to have the values retrieved later in main
	//if the program continued

	//function to perform Least Squares adjustment given the input
	computeLSA();

	//the r_o_i is computed
	compute_r_o_i();
	print("The r_o_i matrix for the coordinates from model space: ");
	print(r_o_i);
	
	//residuals between the r_o_i found and the object space coordinates
	computeResiduals();
	print("The residuals for the r_o_i calculated and the coordinates from model space: ");
	print(residuals);

	//RMSE calculated
	computeRMSE();
	print("The RMSE for this AO: ");
	print(RMSE);
	//higher precision used for the calculations is why i assumed the RMSE is off by +/- 0.03
	//residuals look super close but not rounded like the notes so it effects the RMSE
}

MatrixXd AbsoluteOrientation::computeDelta() {
	//assuming that P = I in this case because we don't have sigma x,y,z...
	MatrixXd P(A.getDesign().rows(), A.getDesign().rows());
	P.setIdentity();
	return -(A.getDesign().transpose() * P * A.getDesign()).inverse() * A.getDesign().transpose() * P * w.getMisclosure();
}


void AbsoluteOrientation::computeLSA() {
	//declare unknowns and set initial values
	unknowns.computeInitialUnknowns(objectCoordinates, modelCoordinates);
	print("Initial Unknown values: ");
	unknowns.printUnknowns();
	
	double averageDelta = 999;

	for (int i = 0; i < 10 && averageDelta > 0.0001; i++) {
		print("-----------------------------------------------------------------");
		print(i+1, "Iteration ");

		//compute the rotation matrix
		print("The rotation matrix: ");
		print(rotationMatrix.computeRotationMatrix(unknowns.getKappa(), unknowns.getPhi(), unknowns.getOmega()));

		//compute the design matrix
		print("The Design Matrix: ");
		print(A.computeDesign(unknowns, modelCoordinates.getCoordinates(), rotationMatrix));

		//compute misclosure vector
		print("The miclosure vector: ");
		print(w.computeMisclosure(unknowns, modelCoordinates, objectCoordinates, rotationMatrix));

		//compute delta
		delta = computeDelta();
		print("The correction to the unknowns: ");
		print(delta);

		//add corrections to the unknowns
		unknowns.updateUnknowns(delta);
		unknowns.printUnknowns();

		//recalculate the convergence paramter
		averageDelta = delta.cwiseAbs().sum()/delta.rows();
		print("Absolute average correction for this iteration: ");
		print(averageDelta);

		print(i+1, "End of iteration ");
		print("-----------------------------------------------------------------");
	}
}

MatrixXd AbsoluteOrientation::compute_r_o_i() {
	Coordinates m = modelCoordinates;//using only m to save space
	r_o_i.resize(m.getCoordinates().rows(), m.getCoordinates().cols());

	//formula for r_o_i used for every point
	for (int i = 0; i < r_o_i.rows(); i++) {
		r_o_i.row(i) = (unknowns.getScale() * rotationMatrix.getRotationMatrix() * m.getCoordinates().row(i).transpose() + unknowns.getTranslationVector()).transpose();
	}

	return r_o_i;
}

MatrixXd AbsoluteOrientation::compute_r_o_i(MatrixXd r_m_i) {
	Coordinates m = modelCoordinates;//m to save space
	MatrixXd r_o_PC(r_m_i.rows(), r_m_i.cols());

	//formula for r_o_i but for every row of points passed
	for (int i = 0; i < r_o_PC.rows(); i++) {
		r_o_PC.row(i) = (unknowns.getScale() * rotationMatrix.getRotationMatrix() * r_m_i.row(i).transpose() + unknowns.getTranslationVector()).transpose();
	}

	return r_o_PC;
}

MatrixXd AbsoluteOrientation::computeResiduals() {
	residuals = r_o_i - objectCoordinates.getCoordinates();

	return residuals;
}

MatrixXd AbsoluteOrientation::computeRMSE() {
	RMSE.resize(1, 3);

	//compute squared residuals
	MatrixXd absResiduals = residuals;
	for (int i = 0; i < residuals.rows(); i++) {
		for (int j = 0; j < residuals.cols(); j++) {
			absResiduals(i, j) = pow(absResiduals(i, j), 2);
		}
	}
	// sqrt(sum r^2 /n)
	RMSE(0, 0) = sqrt(absResiduals.col(0).sum() / residuals.rows());
	RMSE(0, 1) = sqrt(absResiduals.col(1).sum() / residuals.rows());
	RMSE(0, 2) = sqrt(absResiduals.col(2).sum() / residuals.rows());

	return RMSE;
}

MatrixXd AbsoluteOrientation::computeM_i_o(MatrixXd M_i_m) {
	MatrixXd M_o_m = rotationMatrix.getRotationMatrix();
	return M_i_m * M_o_m.transpose();
}

Unknowns AbsoluteOrientation::getUnknowns_AO() {
	return unknowns;
}

Rotation AbsoluteOrientation::getrotationMatrix_AO() {
	return rotationMatrix;
}

MatrixXd AbsoluteOrientation::get_r_o_i_AO() {
	return r_o_i;
}

MatrixXd AbsoluteOrientation::getResiduals_AO() {
	return residuals;
}
