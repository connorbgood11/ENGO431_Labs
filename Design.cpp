#include "Design.h"

Design::Design() {

}

MatrixXd Design::getDesign() {
	return designMatrix;
}

void Design::setDesign(MatrixXd setDesignTo) {
	designMatrix = setDesignTo;
}

MatrixXd Design::computeDesign(Unknowns unknowns, MatrixXd modelCoords, Rotation rotation) {
	designMatrix.resize(3 * modelCoords.rows(), 7);

	//every row of the design matrix is calculated by passing the required parameters to
	//the partial function for the particular row where the rows increment at 3*i pace
	int designRow = 0;
	for (int i = 0; i < modelCoords.rows(); i++) {
		designMatrix.row(designRow++) = computePartialRowX(unknowns, modelCoords.row(i), rotation);
		designMatrix.row(designRow++) = computePartialRowY(unknowns, modelCoords.row(i), rotation);
		designMatrix.row(designRow++) = computePartialRowZ(unknowns, modelCoords.row(i), rotation);
	}

	return designMatrix;
}

MatrixXd Design::computePartialRowX(Unknowns u, MatrixXd m, Rotation r) {
	//where u is unknowns, m is the model coordinates, r is rotation matrix
	MatrixXd partialRow(1, 7);
	
	double k = u.getKappa();
	double o = u.getOmega();
	double l = u.getScale();
	double p = u.getPhi();
	double X = m(0, 0);
	double Y = m(0, 1);
	double Z = m(0, 2);

	//x partial for omega
	partialRow(0, 0) = l * Y * ( -sin(o) * sin(k) + cos(o) * sin(p) * cos(k) )
			+ l * Z *( cos(o) * sin(k) + sin(o) * sin(p) * cos(k) );
	
	//x partial for phi
	partialRow(0, 1) = -l * X * sin(p) * cos(k) + l * Y * sin(o) * cos(p) * cos(k)
		- l * Z * cos(o) * cos(p) * cos(k);

	//x partial for kappa
	partialRow(0, 2) = -l * X * cos(p) * sin(k)
		+ l * Y * ( cos(o) * cos(k) - sin(o) * sin(p) * sin(k) )
		+ l * Z * ( sin(o) * cos(k) + cos(o) * sin(p) * sin(k) );

	//x partial for t x y and z
	partialRow(0, 3) = 1;
	partialRow(0, 4) = 0;
	partialRow(0, 5) = 0;

	//x partial for lambda
	partialRow(0, 6) = X * r.get_m11() + Y * r.get_m12() + Z * r.get_m13();
	

	return partialRow;
}

MatrixXd Design::computePartialRowY(Unknowns u, MatrixXd m, Rotation r) {
	MatrixXd partialRow(1, 7);
	
	double k = u.getKappa();
	double o = u.getOmega();
	double l = u.getScale();
	double p = u.getPhi();
	double X = m(0, 0);
	double Y = m(0, 1);
	double Z = m(0, 2);

	//partial for omega
	partialRow(0, 0) = l * Y * (-sin(o) * cos(k) - cos(o) * sin(p) * sin(k) )
		+ l * Z * ( cos(o) * cos(k) - sin(o) * sin(p) * sin(k) );

	//partial for phi
	partialRow(0, 1) = l * X * sin(p) * sin(k) - l * Y * sin(o) * cos(p) * sin(k)
		+ l * Z * cos(o) * cos(p) * sin(k);

	//partial for kappa
	partialRow(0, 2) = -l * X * cos(p) * cos(k)
		+l * Y * ( -cos(o) * sin(k) - sin(o) * sin(p) * cos(k) )
		+l * Z * ( -sin(o) * sin(k) + cos(o) * sin(p) * cos(k) );

	//partials for t x,y,z
	partialRow(0, 3) = 0;
	partialRow(0, 4) = 1;
	partialRow(0, 5) = 0;

	//partial for lambda
	partialRow(0, 6) = X * r.get_m21() + Y * r.get_m22() + Z * r.get_m23();

	
	return partialRow;
}

MatrixXd Design::computePartialRowZ(Unknowns u, MatrixXd m, Rotation r) {
	MatrixXd partialRow(1, 7);
	
	double k = u.getKappa();
	double o = u.getOmega();
	double l = u.getScale();
	double p = u.getPhi();
	double X = m(0, 0);
	double Y = m(0, 1);
	double Z = m(0, 2);

	//partial for omega
	partialRow(0, 0) = -l* Y * cos(o) * cos(p) - l * Z * sin(o) * cos(p);

	//partial for phi
	partialRow(0, 1) = l * X * cos(p) + l * Y * sin(o) * sin(p)
		- l * Z * cos(o) * sin(p);

	//partial for kappa
	partialRow(0, 2) = 0;

	//partials for t x,y,z
	partialRow(0, 3) = 0;
	partialRow(0, 4) = 0;
	partialRow(0, 5) = 1;

	//partial for lambda
	partialRow(0, 6) = X * r.get_m31() + Y * r.get_m32() + Z * r.get_m33();

	return partialRow;
}