All formulas used come from AO notes and rotation notes

Class AbsoluteOrientation can take in object and image space coordinates
and generate the AO parameters to be used for calculating object space
coordinates and other required values from the lab handout

AO does all the AO related computations while the rest of the different
classes have their own classes with member function to get the data ready
for AO computations