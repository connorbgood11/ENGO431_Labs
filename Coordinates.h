#pragma once
#include "header.h"

class Coordinates {
private:
	MatrixXd coordinates;
	MatrixXd X;
	MatrixXd Y;
	MatrixXd Z;

public:
	//constructor for  coordinates that takes in a filename
	Coordinates();

	//getters for  coordinates
	MatrixXd getCoordinates();
	MatrixXd getX();
	MatrixXd getY();
	MatrixXd getZ();

	//setters for  coordinates
	void setCoordinates(MatrixXd coords);
	void setX(MatrixXd X_vals);
	void setY(MatrixXd Y_vals);
	void setZ(MatrixXd Z_vals);

	//splitter function to separate coordinates to x y z
	void splitCoordinates();

	//retrieves  coordinates from file
	void loadCoordinates(string filename);
};