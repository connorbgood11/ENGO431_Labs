#pragma once
#include "header.h"
#include "Coordinates.h"
#include "Design.h"
#include "Misclosure.h"
#include "Unknowns.h"

class AbsoluteOrientation {
private:
	Unknowns unknowns;
	Rotation rotationMatrix;
	Design A;
	Misclosure w;
	MatrixXd delta;
	MatrixXd r_o_i;
	MatrixXd residuals;
	Coordinates modelCoordinates;
	Coordinates objectCoordinates;
	MatrixXd RMSE;

public:
	//constructor
	AbsoluteOrientation();

	//function to control all of the AO
	void computeAO(string filename_objectCoords, string filename_ModelCoords);

	//function to compute the lsa corrections
	MatrixXd computeDelta();

	//function to control the LSA
	void computeLSA();

	//calculate the r_o_i aka the transformed points
	MatrixXd compute_r_o_i();
	//overloaded version for any new set of points once the AO parameters are established
	MatrixXd compute_r_o_i(MatrixXd r_m_i);

	//calculte the residuals
	MatrixXd computeResiduals();
	MatrixXd computeRMSE();

	//calculate M_i_o
	MatrixXd computeM_i_o(MatrixXd M_i_m);

	//getters for member variables that may be used later or analyzed
	//added _AO so that the class function for some variables aren't accidentally called
	Unknowns getUnknowns_AO();
	Rotation getrotationMatrix_AO();
	MatrixXd get_r_o_i_AO();
	MatrixXd getResiduals_AO();
};