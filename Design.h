#pragma once
#include "header.h"
#include "Unknowns.h"
#include "Rotation.h"

class Design {
private:
	MatrixXd designMatrix;

public:
	//constructor of design matrix that take in unknown parameters
	//ground control points where columns are x, y, z
	Design();

	//getter for design matrix
	MatrixXd getDesign();

	//setter for design matrix
	void setDesign(MatrixXd setDesignTo);

	//function to compute the design matrix using the unknowns
	MatrixXd computeDesign(Unknowns unknowns, MatrixXd modelCoords, Rotation rotation);

	//compute the row partial for the design matrix
	MatrixXd computePartialRowX(Unknowns u, MatrixXd m, Rotation r);
	MatrixXd computePartialRowY(Unknowns u, MatrixXd m, Rotation r);
	MatrixXd computePartialRowZ(Unknowns u, MatrixXd m, Rotation r);
	
};