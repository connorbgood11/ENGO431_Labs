#include "Coordinates.h"

Coordinates::Coordinates() {

}

MatrixXd Coordinates::getCoordinates() {
	return coordinates;
}

MatrixXd Coordinates::getX() {
	return X;
}

MatrixXd Coordinates::getY() {
	return Y;
}

MatrixXd Coordinates::getZ() {
	return Z;
}

void Coordinates::setCoordinates(MatrixXd coords) {
	coordinates = coords;
}

void Coordinates::setX(MatrixXd X_vals) {
	X = X_vals;
}

void Coordinates::setY(MatrixXd Y_vals) {
	Y = Y_vals;
}

void Coordinates::setZ(MatrixXd Z_vals) {
	Z = Z_vals;
}

void Coordinates::splitCoordinates() {
	//each  coordinate is assigned its own vector for easier naming and accessing
	X = coordinates.col(0);
	Y = coordinates.col(1);
	Z = coordinates.col(2);
}

void Coordinates::loadCoordinates(string filename) {
	coordinates = loadMatrix(filename);
}