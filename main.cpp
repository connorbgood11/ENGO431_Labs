#include "header.h"
#include "ClassHeaders.h"

int main(){
	//README.txt contains brief overview of the code

	//START TEST COMPUTATIONS----------------------------------------------
	//gets the model from a set of model coordinates and object coordinates
	
	AbsoluteOrientation testAO;
	testAO.computeAO("testObjectCoords.txt", "testModelCoords.txt");
	
	//LEFT MODEL test calculations
	//testing the left and right model space coordinates to object space
	MatrixXd r_o_testLeftModel = testAO.compute_r_o_i(loadMatrix("testLeftModel.txt"));
	print("Test left model to object space");
	print(r_o_testLeftModel);
	//now calculating the rotation matrix from object space to image space
	Rotation M_i_o_left;
	M_i_o_left.setRotationMatrix(testAO.computeM_i_o(loadMatrix("leftM_i_m.txt")));
	print("The left model rotation to image from object matrix: ");
	print(M_i_o_left.getRotationMatrix());
	print("The extracted angles for Left Omega, Phi, and Kappa in degrees are: ");
	print(M_i_o_left.extractDegreeAngles());
	
	//RIGHT MODEL test calculations
	MatrixXd r_o_testRightModel = testAO.compute_r_o_i(loadMatrix("testRightModel.txt"));
	print("Test right model to object space");
	print(r_o_testRightModel);
	Rotation M_i_o_right;
	M_i_o_right.setRotationMatrix(testAO.computeM_i_o(loadMatrix("rightM_i_m.txt")));
	print("The right model rotation to image from object matrix: ");
	print(M_i_o_right.getRotationMatrix());
	print("The extracted angles for Right Omega, Phi, and Kappa in degrees are: ");
	print(M_i_o_right.extractDegreeAngles());
	
	//END TEST COMPUTATIONS------------------------------------------------

	//START OUR COMPUTATIONS-----------------------------------------------

	//AbsoluteOrientation AO;
	//AO.computeAO("ourObjectCoordsAreInTextFile", "ourModelCoordinatesInATextFile");

	//END OUR COMPUTATIONS-------------------------------------------------
}