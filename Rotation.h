#pragma once
#include "header.h"

class Rotation {
private:
	MatrixXd rotationMatrix;

public:
	//getter
	MatrixXd getRotationMatrix();

	//setter
	void setRotationMatrix(MatrixXd rotationMatToSet);

	//compute the rotation matrix from the unknowns
	MatrixXd computeRotationMatrix(double kappa, double phi, double omega);

	//retrieve elements from rotation matrix by name
	//makes it easier to read than indexing through the rotation matrix
	double get_m11();
	double get_m12();
	double get_m13();

	double get_m21();
	double get_m22();
	double get_m23();

	double get_m31();
	double get_m32();
	double get_m33();

	//compute the R3 rotation matrix for an angle
	MatrixXd computeR3(double angle);

	//takes the omega phi and kappa angles out of the rotation matrix
	MatrixXd extractDegreeAngles();
	double extractOmega();
	double extractPhi();
	double extractKappa();
};