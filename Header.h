#pragma once

#include <iostream>
#include <fstream>

#include "Eigen"
#include <cmath>
#include <vector>
#include <iomanip>
# define M_PI 3.14159265358979323846

using namespace std;
using namespace Eigen;

void print(string text);//prints a string of text

void print(MatrixXd matToPrint);//prints out a matrix

void print(double doubleToPrint);//prints out a double

MatrixXd loadMatrix(string filename);//reads a matrix from txt and stores as matrix

void print(int intToPrint, string text);//prints text and an int