#pragma once
#include "header.h"
#include "Unknowns.h"
#include "Coordinates.h"
#include "Rotation.h"

class Misclosure {
private:
	MatrixXd misclosureVector;

public:
	//constructor of misclosure vector that take in unknown parameters
	Misclosure();

	//getter for misclosure vector
	MatrixXd getMisclosure();

	//setter for misclosure vector
	void setMisclosure(MatrixXd setMisclosureTo);

	//function to compute the misclosure vector
	MatrixXd computeMisclosure(Unknowns u, Coordinates m, Coordinates o, Rotation r);

};