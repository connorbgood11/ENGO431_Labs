#include "Misclosure.h"

Misclosure::Misclosure() {

}

MatrixXd Misclosure::getMisclosure() {
	return misclosureVector;
}

void Misclosure::setMisclosure(MatrixXd setMisclosureTo) {
	misclosureVector = setMisclosureTo;
}

MatrixXd Misclosure::computeMisclosure(Unknowns u, Coordinates m, Coordinates o, Rotation r) {//u = unknowns, m = model coords, o = object coords, r = rotation matrix
	misclosureVector.resize(3 * (o.getCoordinates()).rows(), 1);
	
	double l = u.getScale(); //lambda
	double t_x = u.get_t_x();//t values
	double t_y = u.get_t_y();
	double t_z = u.get_t_z();

	//misclosure function applied for every row where row count increase at a rate of 3*i
	//formula followed for the values of each row
	int misclosureRow = 0;
	for (int i = 0; i < (o.getCoordinates()).rows(); i++) {
		misclosureVector(misclosureRow++, 0) = l * (r.get_m11() * m.getX()(i, 0) + r.get_m12() * m.getY()(i, 0) + r.get_m13() * m.getZ()(i, 0)) + t_x - o.getX()(i, 0);
		misclosureVector(misclosureRow++, 0) = l * (r.get_m21() * m.getX()(i, 0) + r.get_m22() * m.getY()(i, 0) + r.get_m23() * m.getZ()(i, 0)) + t_y - o.getY()(i, 0);
		misclosureVector(misclosureRow++, 0) = l * (r.get_m31() * m.getX()(i, 0) + r.get_m32() * m.getY()(i, 0) + r.get_m33() * m.getZ()(i, 0)) + t_z - o.getZ()(i, 0);
	}

	return misclosureVector;
}