#pragma once
#include "header.h"
#include "Coordinates.h"
#include "Rotation.h"

class Unknowns {
private:
	//variables to be stored in unknowns
	double omega = 0;
	double phi = 0;
	double kappa = 0;
	double t_x = 0;
	double t_y = 0;
	double t_z = 0;
	double scale = 0;

	MatrixXd translationVector;

public:
	//constructor
	Unknowns();

	//getters for unknowns
	double getOmega();
	double getOmegaDeg();
	double getPhi();
	double getPhiDeg();
	double getKappa();
	double getKappaDeg();
	double get_t_x();
	double get_t_y();
	double get_t_z();
	double getScale();

	//setters for unknowns
	void setOmega(double o);
	void setPhi(double p);
	void setKappa(double k);
	void set_t_x(double t);
	void set_t_y(double t);
	void set_t_z(double t);
	void setScale(double s);

	//function to retrieve translation vector
	MatrixXd getTranslationVector();

	//function to compute the initial unknowns from the initial unknowns functions
	void computeInitialUnknowns(Coordinates object, Coordinates model);

	//function to compute initial approx of lambda
	double computeInitialLambda(Coordinates object, Coordinates model);
	
	//function to compute initial Kappa
	double computeInitialKappa(Coordinates object, Coordinates model);

	//compute the initial t values
	void computeInitial_t_values(Coordinates object, Coordinates model);

	//update the unknowns with the corrections
	void updateUnknowns(MatrixXd corrections);

	//print unknowns
	void printUnknowns();
};