#include "Rotation.h"

MatrixXd Rotation::getRotationMatrix() {
	return rotationMatrix;
}

void Rotation::setRotationMatrix(MatrixXd rotationMatToSet) {
	rotationMatrix = rotationMatToSet;
}

MatrixXd Rotation::computeRotationMatrix(double kappa, double phi, double omega) {
	rotationMatrix.resize(3, 3);

	//row 1
	rotationMatrix(0, 0) = cos(phi) * cos(kappa);//m11
	rotationMatrix(0, 1) = cos(omega) * sin(kappa) + sin(omega) * sin(phi) * cos(kappa);//m12
	rotationMatrix(0, 2) = sin(omega) * sin(kappa) - cos(omega) * sin(phi) * cos(kappa);//m13

	//row 2
	rotationMatrix(1, 0) = -cos(phi) * sin(kappa);//m21
	rotationMatrix(1, 1) = cos(omega) * cos(kappa) - sin(omega) * sin(phi) * sin(kappa);//m22
	rotationMatrix(1, 2) = sin(omega) * cos(kappa) + cos(omega) * sin(phi) * sin(kappa);//m23

	//row 3
	rotationMatrix(2, 0) = sin(phi);//m31
	rotationMatrix(2, 1) = -sin(omega) * cos(phi);//m32
	rotationMatrix(2, 2) = cos(omega) * cos(phi);//m33

	return rotationMatrix;
}

//row 1
double Rotation::get_m11() {
	return rotationMatrix(0, 0);
}
double Rotation::get_m12() {
	return rotationMatrix(0, 1);
}
double Rotation::get_m13() {
	return rotationMatrix(0, 2);
}

//row 2
double Rotation::get_m21() {
	return rotationMatrix(1, 0);
}
double Rotation::get_m22() {
	return rotationMatrix(1, 1);
}
double Rotation::get_m23() {
	return rotationMatrix(1, 2);
}

//row 3
double Rotation::get_m31() {
	return rotationMatrix(2, 0);
}
double Rotation::get_m32() {
	return rotationMatrix(2, 1);
}
double Rotation::get_m33() {
	return rotationMatrix(2, 2);
}

MatrixXd Rotation::computeR3(double angle) {
	MatrixXd R3(3, 3);
	R3.setZero();

	//row 1
	R3(0, 0) = cos(angle);
	R3(0, 1) = sin(angle);
	
	//row 2
	R3(1, 0) = -sin(angle);
	R3(1, 1) = cos(angle);

	//row 3
	R3(2, 2) = 1;

	return R3;
}

MatrixXd Rotation::extractDegreeAngles() {
	MatrixXd angles(3, 1);

	//compute the angles in degrees
	angles(0, 0) = extractOmega() * 180 / M_PI;
	angles(1, 0) = extractPhi() * 180 / M_PI;
	angles(2, 0) = extractKappa() * 180 / M_PI;

	return angles;
}

double Rotation::extractOmega() {
	return atan(-get_m32() / get_m33());
}

double Rotation::extractPhi() {
	return asin(get_m31());
}

double Rotation::extractKappa() {
	return atan(-get_m21() / get_m11());
}