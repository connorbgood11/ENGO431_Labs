#include "Unknowns.h"

//constructor
Unknowns::Unknowns() {

}

//getters
double Unknowns::getOmega() {
	return omega;
}

double Unknowns::getOmegaDeg() {
	return omega * 180 / M_PI;
}

double Unknowns::getKappa() {
	return kappa;
}

double Unknowns::getKappaDeg() {
	return kappa * 180 / M_PI;
}

double Unknowns::getPhi() {
	return phi;
}

double Unknowns::getPhiDeg() {
	return phi * 180 / M_PI;
}

double Unknowns::getScale() {
	return scale;
}

double Unknowns::get_t_x() {
	return t_x;
}

double Unknowns::get_t_y() {
	return t_y;
}

double Unknowns::get_t_z() {
	return t_z;
}

//setters
void Unknowns::setKappa(double k) {
	kappa = k;
}

void Unknowns::setOmega(double o) {
	omega = o;
}

void Unknowns::setPhi(double p) {
	phi = p;
}

void Unknowns::set_t_x(double t) {
	t_x = t;
}

void Unknowns::set_t_y(double t) {
	t_y = t;
}

void Unknowns::set_t_z(double t) {
	t_z = t;
}

void Unknowns::setScale(double s) {
	scale = s;
}

//class functions
MatrixXd Unknowns::getTranslationVector() {
	//compute the translation vector from the Unknown member variables
	translationVector.resize(3, 1);
	translationVector(0, 0) = t_x;
	translationVector(1, 0) = t_y;
	translationVector(2, 0) = t_z;

	return translationVector;
}

void Unknowns::computeInitialUnknowns(Coordinates object, Coordinates model) {
	//initial value functions called
	computeInitialKappa(object, model);
	computeInitialLambda(object, model);
	computeInitial_t_values(object, model);
}

double Unknowns::computeInitialLambda(Coordinates object, Coordinates model) {
	double i = 0;
	double j = 1;

	//formula from page 8 notes, last zi said object but i think it meant model*** changing it either way makes no different because the iterations get rid of the different*tested
	scale = sqrt( pow(object.getX()(j, 0) - object.getX()(i, 0), 2) + pow(object.getY()(j, 0) - object.getY()(i, 0), 2) + pow(object.getZ()(j, 0) - object.getZ()(i, 0), 2) ) /
		sqrt( pow(model.getX()(j, 0) - model.getX()(i, 0), 2) + pow(model.getY()(j, 0) - model.getY()(i, 0), 2) + pow(model.getZ()(j, 0) - model.getZ()(i, 0), 2) );

	return scale;
}

double Unknowns::computeInitialKappa(Coordinates object, Coordinates model) {
	double i = 0;
	double j = 1;
	
	//kappa formula applied
	kappa = atan2(object.getX()(j, 0) - object.getX()(i, 0), object.getY()(j, 0) - object.getY()(i, 0))
		- atan2(model.getX()(j, 0) - model.getX()(i, 0), model.getY()(j, 0) - model.getY()(i, 0));

	return kappa;
}

void Unknowns::computeInitial_t_values(Coordinates object, Coordinates model) {
	//formula for computing t values applied
	MatrixXd r_o_i(3, 1);
	MatrixXd r_m_i(3, 1);

	r_o_i(0, 0) = object.getX()(0, 0);
	r_o_i(1, 0) = object.getY()(0, 0);
	r_o_i(2, 0) = object.getZ()(0, 0);

	r_m_i(0, 0) = model.getX()(0, 0);
	r_m_i(1, 0) = model.getY()(0, 0);
	r_m_i(2, 0) = model.getZ()(0, 0);

	Rotation r;
	MatrixXd R3_k = r.computeR3(kappa);

	translationVector.resize(3, 1);
	translationVector = r_o_i - scale * R3_k * r_m_i;

	t_x = translationVector(0, 0);
	t_y = translationVector(1, 0);
	t_z = translationVector(2, 0);
}

void Unknowns::updateUnknowns(MatrixXd corrections) {
	//corrections added to the unknowns
	omega += corrections(0, 0);
	phi += corrections(1, 0);
	kappa += corrections(2, 0);
	t_x += corrections(3, 0);
	t_y += corrections(4, 0);
	t_z += corrections(5, 0);
	scale += corrections(6, 0);
}

void Unknowns::printUnknowns() {
	//print all the unknowns
	print("Omega in radians: ");
	print(omega);
	print("Omega in degrees: ");
	print(getOmegaDeg());
	print("Phi in radians: ");
	print(phi);
	print("Phi in degrees: ");
	print(getPhiDeg());
	print("Kappa in radians: ");
	print(kappa);
	print("Kappa in degrees: ");
	print(getKappaDeg());
	print("t_x: ");
	print(t_x);
	print("t_y:");
	print(t_y);
	print("t_z: ");
	print(t_z);
	print("Scale(Lambda): ");
	print(scale);
}