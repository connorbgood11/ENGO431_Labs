#include "header.h"

void print(string text) {
	cout << text << endl;
}

void print(MatrixXd matToPrint) {
	for (int i = 0; i < matToPrint.rows(); i++) {
		for (int j = 0; j < matToPrint.cols(); j++) {
			cout << setprecision(8) << matToPrint(i, j) << "\t";
		}
		cout << endl;
	}
	cout << endl;
}

void print(double doubleToPrint) {
	cout << setprecision(8) << doubleToPrint << endl << endl;
}

void print(int intToPrint, string text) {
	cout << text << intToPrint << endl << endl;
}

MatrixXd loadMatrix(string filename) {
	//reads file in matrix format
	//the rows with a space and then columns comes before the matrix

	ifstream file_in;

	file_in.open(filename, ifstream::in);

	if (file_in.fail()) {
		cout << "An error has occured while trying to open the input stream for " << filename << endl;
	}

	int numberOfRows;
	int numberOfCols;

	file_in >> numberOfRows;
	file_in >> numberOfCols;

	MatrixXd readMatrix(numberOfRows, numberOfCols);

	while (!file_in.eof()) {
		for (int i = 0; i < readMatrix.rows(); i++) {
			for (int j = 0; j < readMatrix.cols(); j++) {
				file_in >> readMatrix(i, j);
			}
		}
	}

	file_in.close();

	return readMatrix;
}